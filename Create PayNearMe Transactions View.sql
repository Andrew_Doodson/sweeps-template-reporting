-- View: sweeps.pay_near_me_transactions_view

-- DROP VIEW sweeps.pay_near_me_transactions_view;

CREATE OR REPLACE VIEW sweeps.pay_near_me_transactions_view
 AS
 SELECT a.action_id,
    a.created_at,
    a.last_modified_at,
    a.order_identifier,
    a.payment_method,
    a.pay_near_me_status,
    b.email,
    d.cash_amount,
    d.payment_provider,
    d.status AS donation_status,
    c.status AS action_status
   FROM sweeps.pay_near_me a,
    sweeps.users b,
    sweeps.user_actions c,
    sweeps.payments d
  WHERE a.action_id = c.action_id AND b.user_id::text = c.user_id::text AND a.action_id = d.action_id
  ORDER BY a.created_at DESC;

ALTER TABLE sweeps.pay_near_me_transactions_view
    OWNER TO rubyplay;

GRANT SELECT ON TABLE sweeps.pay_near_me_transactions_view TO rubyplay;
GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE sweeps.pay_near_me_transactions_view TO rubyplay;
GRANT ALL ON TABLE sweeps.pay_near_me_transactions_view TO rubyplay;

