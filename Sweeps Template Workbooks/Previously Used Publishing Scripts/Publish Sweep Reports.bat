@echo off
SETLOCAL EnableDelayedExpansion

set /p tablogin="Enter Tableau Online Login Email: " 
set /p tabpass="Enter Tableau Online Login Password: "
set /p dbpass="Enter DB Password: "

set scriptpath=%~dp0

tabcmd login -s https://10ay.online.tableau.com/#/site/cashed -u %tablogin% -p %tabpass%

tabcmd publish "%scriptpath%Transactions Report - Live - SWEEPS.twb" -o --db-password !dbpass! -save-db-password  --parent-project-path "New Structure/Loyal Royal/Working" -r "Live" --tabbed
tabcmd publish "%scriptpath%Big Wins - Live - Sweeps.twb" -o --db-password !dbpass! -save-db-password --parent-project-path "New Structure/Loyal Royal/Working" -r "Live" --tabbed
tabcmd publish "%scriptpath%Billing Report - Live - SWEEPS.twb" -o --db-password !dbpass! -save-db-password --parent-project-path "New Structure/Loyal Royal/Working" -r "Live" --tabbed
tabcmd publish "%scriptpath%Daily Stats On Players - Live - Sweeps.twb" -o --db-password !dbpass! -save-db-password --parent-project-path "New Structure/Loyal Royal/Working" -r "Live" --tabbed
tabcmd publish "%scriptpath%Game Analysis (Mikayla) - Live - SWEEPS.twb" -o  --db-password !dbpass! -save-db-password --parent-project-path "New Structure/Loyal Royal/Working" -r "Live" --tabbed
tabcmd publish "%scriptpath%Game Performance - Live - SWEEPS.twb" -o --db-password !dbpass! -save-db-password  --parent-project-path "New Structure/Loyal Royal/Working" -r "Live" --tabbed
tabcmd publish "%scriptpath%Player LTV - Live - SWEEPS.twb" -o --db-password !dbpass! -save-db-password  --parent-project-path "New Structure/Loyal Royal/Working" -r "Live" --tabbed
tabcmd publish "%scriptpath%Promo Code Report - Live - SWEEPS.twb" -o --db-password !dbpass! -save-db-password  --parent-project-path "New Structure/Loyal Royal/Working" -r "Live" --tabbed
tabcmd publish "%scriptpath%Client KPI  - Sweeps - Live.twb" -o --db-password !dbpass! -save-db-password  --parent-project-path "New Structure/Loyal Royal/Working" -r "Live" --tabbed
tabcmd publish "%scriptpath%Internal KPI  - Sweeps - Live.twb" -o --db-password !dbpass! -save-db-password  --parent-project-path "New Structure/Loyal Royal/Working" -r "Live" --tabbed
tabcmd publish "%scriptpath%Onboarding - Sweeps - Live.twb" -o --db-password !dbpass! -save-db-password  --parent-project-path "New Structure/Loyal Royal/Working" -r "Live" --tabbed
tabcmd publish "%scriptpath%Ekata - Risk Profile - Sweeps - Live.twb" -o --db-password !dbpass! -save-db-password  --parent-project-path "New Structure/Loyal Royal/Working" -r "Live" --tabbed

pause