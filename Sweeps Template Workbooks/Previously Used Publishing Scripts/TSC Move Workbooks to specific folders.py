# -*- coding: utf-8 -*-
"""
Created on Tue Mar 15 14:25:42 2022

@author: Ruby
This one is for sweeps workbooks.

BIG ISSUE, CANNOT MOVE WORKBOOKS TO A PROJECT IF THE PROJECT HAS A WORKBOOK WITH THE SAME NAME
AM GOING TO ADAPT THIS AND THE PUBLISH COMMAND TOGETHER.
"""
import tableauserverclient as TSC


#When the token changes we only change here intsead of throughout the code
token_name = 'TSC'
token_value = '5cY0iNZJTcSedD585CjOsA==:JveYUypi9jupNsfHW3bcEF2H8T5xygFn'
site_name = 'cashed'


#Here we will make lists of each workbook that belongs in a project
#This means if there are new reports that need publishing you update here only 

#internal financials 
internal_financials = ["Internal KPI  - SWEEPS", "Additional KPI Metrics - SWEEPS"]

#internal games
internal_games = ["Game Analysis - SWEEPS", "Search for Specific Results - SWEEPS",
                  "User Balance Over Time - SWEEPS", "Daily Stats - SWEEPS", "Jackpots - SWEEPS"]

#valuation
valuation = ["Daily Stats On Players - SWEEPS", "Game Performance - SWEEPS", "Player LTV - SWEEPS",
             "VIP Users - SWEEPS", "Users to Target for Points - SWEEPS", "Outstanding Credit Bets - SWEEPS"]

#Client Financials 
client_financials = ["Billing Report - SWEEPS", "Daily Redemptions - SWEEPS",
                     'Daily Donations By Player - SWEEPS', 'Transactions Report - SWEEPS']

promotions = ["Player Nicknames - SWEEPS", "Promo Code Report - SWEEPS" ]


#Make a list of the sections. We will eventually use this to convert the below
#work into a loop that goes through all of the sections...
list_of_sections = [internal_financials, internal_games, valuation, client_financials, promotions]

####################################################################################################################

#Get the folder that the workbooks are currently in 
initial_project_name = input("What folder are the workbooks that need to be moved currently located in: ")

#Get the abbreviation of the project that we want to move the workbooks into
#e.g. Strawberry or Loyal Royal
final_project_name = input("Enter the abreviation project name that you wish to move files to: ")

#This is our way of signining in using PAT's 
tableau_auth = TSC.PersonalAccessTokenAuth(token_name, token_value, site_name )
server = TSC.Server('https://10ay.online.tableau.com/')

with server.auth.sign_in(tableau_auth):
    #Get all of the projects on the site 
    all_projects = TSC.Pager(server.projects)
    #Get all of the workbooks on the site 
    all_wbs = TSC.Pager(server.workbooks)
    
    working_project = next((project for project in all_projects if project.name == initial_project_name), None)
  

    
    #If the project the user inputted exists:
    if working_project is not None:
        #if the desired project to move to exists
        print('Found project {0}:'.format(working_project.name))
        print()
        print('Project ID is: {0}'.format(working_project.id))
                
        print("Updating project workbooks now...")
        #Filter the wbs list into only the workbooks we want to update
        #If the workbook project id = the working project id we want to update it
        workbooks_in_original_project = [wb for wb in all_wbs if wb.project_id == working_project.id]
        
        #If the workbook is in the games we want to move it to the games portion
        game_workbooks_to_update = [wb for wb in workbooks_in_original_project if wb.name in internal_games]
        all_projects = TSC.Pager(server.projects)
        final_project_name_game = final_project_name + ' Games'
        final_project = next((project for project in all_projects if project.name == final_project_name_game), None)
        
        for wb in game_workbooks_to_update:
            #For some reason we have to get all the projects again to make the second project be found 

            print(final_project_name)
            print("Workbook: {0}".format(wb.name))
            print("Old project: {}".format(wb.project_name))
            wb.project_id = final_project.id
            target_workbook = server.workbooks.update(wb)
            print("New project: {}".format(target_workbook.project_name))
            print("")
            
            
        #If the workbook is in the valuation workbooks we want to move it to that section
        all_projects = TSC.Pager(server.projects)
        final_project_name_value = final_project_name + ' Valuation'
        final_project = next((project for project in all_projects if project.name == final_project_name_value), None)    
        valuation_workbooks_to_update = [wb for wb in workbooks_in_original_project if wb.name in valuation]

        for wb in valuation_workbooks_to_update:
            print("Workbook: {0}".format(wb.name))
            print("Old project: {}".format(wb.project_name))
            wb.project_id = final_project.id
            target_workbook = server.workbooks.update(wb)
            print("New project: {}".format(target_workbook.project_name))
            print("")
            
        #If the workbook name matches the list above we want to move it
        all_projects = TSC.Pager(server.projects)
        final_project_name_fin = final_project_name + ' Financials'
        final_project = next((project for project in all_projects if project.name == final_project_name_fin), None)    
        int_fin_workbooks_to_update = [wb for wb in workbooks_in_original_project if wb.name in internal_financials]
        
        for wb in int_fin_workbooks_to_update:
            print("Workbook: {0}".format(wb.name))
            print("Old project: {}".format(wb.project_name))
            wb.project_id = final_project.id
            target_workbook = server.workbooks.update(wb)
            print("New project: {}".format(target_workbook.project_name))
            print("")
           
        all_projects = TSC.Pager(server.projects)
        final_project_name_fin = final_project_name + ' Financials'
        final_project = next((project for project in all_projects if project.name == final_project_name_fin), None)    
        cli_fin_workbooks_to_update = [wb for wb in workbooks_in_original_project if wb.name in client_financials]
        for wb in cli_fin_workbooks_to_update:
            print("Workbook: {0}".format(wb.name))
            print("Old project: {}".format(wb.project_name))
            wb.project_id = final_project.id
            target_workbook = server.workbooks.update(wb)
            print("New project: {}".format(target_workbook.project_name))
            print("")
        
        all_projects = TSC.Pager(server.projects)
        final_project_name_promo = final_project_name + ' Promotions'
        final_project = next((project for project in all_projects if project.name == final_project_name_promo), None)    
        promo_workbooks_to_update = [wb for wb in workbooks_in_original_project if wb.name in promotions]
        for wb in promo_workbooks_to_update:
            print("Workbook: {0}".format(wb.name))
            print("Old project: {}".format(wb.project_name))
            wb.project_id = final_project.id
            target_workbook = server.workbooks.update(wb)
            print("New project: {}".format(target_workbook.project_name))
            print("")
            
            
            
            
            
        #Sign out of the server
        server.auth.sign_out()
        print ("Successfully logged out of server")
        
        
        
    
    #If the first project the user inputted does not exist 
    else:
        print("Initial project not found")