# -*- coding: utf-8 -*-
"""
Created on Thu Mar  3 08:18:05 2022

@author: Andrew Doodson
"""


import tableauserverclient as TSC
#from tableauserverclient import ConnectionCredentials, ConnectionItem
import os



#Establish the filepath and the file that we will be using - this is the directory that the script is in
rootdir =  os.path.dirname(__file__)

#Code token and site credentials here
#When the token changes we only change here intsead of throughout the code
token_name = 'TSC'
token_value = 'qkgTthAMSeqgQRSTsyL1hQ==:004vNsZANu020Lb6fJGWovptIOBYDmCu'
site_name = 'cashed'

#Have the user input the DB password so that it isn't hard coded
#b_pass = input("Enter DB Password: ")

#This is our way of signining in using PAT's 
tableau_auth = TSC.PersonalAccessTokenAuth(token_name, token_value, site_name )
server = TSC.Server('https://10ay.online.tableau.com/')



with server.auth.sign_in(tableau_auth):
    #Verify that you've logged in successfully
    print("Successfully logged into server")
    
    # Step 1: Get all the projects on server, then look for the default one.
    all_projects, pagination_item = server.projects.get()
    default_project = next((project for project in all_projects if project.is_default()), None)
    
    #Step 2, get all workbooks on the server
    all_workbooks, pagination_item = server.workbooks.get()

    #Step 3: Get all of the workbooks in the project 
    
    if default_project is not None:
        #Get only the workbooks that are in the project that we're interested in.
        workbooks_for_project = [wb for wb in all_workbooks if workbook.project_id = [default_project.id]]
        for workbook in workbooks_for_project:
            print("\nUpdate {0} workbook".format(workbook.name))
            workbook.show_tabs = True
            # Call the update method
            workbook = server.workbooks.update(workbook)
            print("\nUpdated {0} workbook".format(workbook.name))
            
    else:
        print('Default project not found')

    
#Sign out of the server
server.auth.sign_out()
print ("Successfully logged out of server")

    
            
            
