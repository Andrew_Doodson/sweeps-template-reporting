@echo off
SETLOCAL EnableDelayedExpansion
::Get Tableau login information from the user
set /p tablogin="Enter Tableau Online Login Email: " 
set /p tabpass="Enter Tableau Online Login Password: "
set /p dbpass="Enter DB Password: "
::Set the scriptpath to be equal to the folder that the batch file is running in.
set scriptpath=%~dp0

::Login to the site
tabcmd login -s https://10ay.online.tableau.com/#/site/cashed -u %tablogin% -p %tabpass%

::Publish the marketing reports to the INTERNAL MARKETING area
::Wins
tabcmd publish "%scriptpath%Big Wins - SWEEPS.twb" -o --db-password !dbpass! -save-db-password --parent-project-path "New Structure/Loyal Royal/Internal/Marketing" -r "Wins" --tabbed

::Events
::Below doesnt exist for sweeps so it is commented out
::tabcmd publish "%scriptpath%Credit Amount Leaderboard - SWEEPS.twb" -o --db-password !dbpass! -save-db-password  --parent-project-path "New Structure/Loyal Royal/Internal/Marketing" -r "Events" --tabbed

::Promotions
tabcmd publish "%scriptpath%Player Nicknames - SWEEPS.twb" -o --db-password !dbpass! -save-db-password  --parent-project-path "New Structure/Loyal Royal/Internal/Marketing" -r "Promotions" --tabbed
tabcmd publish "%scriptpath%Promo Code Report - SWEEPS.twb" -o --db-password !dbpass! -save-db-password  --parent-project-path "New Structure/Loyal Royal/Internal/Marketing" -r "Promotions" --tabbed

::Doesn't exist, needs making
::tabcmd publish "%scriptpath%Bonus Points Given Away - SWEEPS.twb" -o --db-password !dbpass! -save-db-password  --parent-project-path "New Structure/Loyal Royal/Internal/Marketing" -r "Promotions" --tabbed

::Publish the Players reports to the INTERNAL PLAYERS  area
::Risk
tabcmd publish "%scriptpath%Ekata - Risk Profile - SWEEPS.twb" -o --db-password !dbpass! -save-db-password --parent-project-path "New Structure/Loyal Royal/Internal/Players" -r "Risk" --tabbed

::Funnel
tabcmd publish "%scriptpath%Onboarding - SWEEPS.twb" -o --db-password !dbpass! -save-db-password  --parent-project-path "New Structure/Loyal Royal/Internal/Players" -r "Funnel" --tabbed

::Valuation
tabcmd publish "%scriptpath%Daily Stats On Players - SWEEPS.twb" -o --db-password !dbpass! -save-db-password --parent-project-path "New Structure/Loyal Royal/Internal/Players" -r "Valuation" --tabbed
tabcmd publish "%scriptpath%Game Performance - SWEEPS.twb" -o --db-password !dbpass! -save-db-password  --parent-project-path "New Structure/Loyal Royal/Internal/Players" -r "Valuation" --tabbed
tabcmd publish "%scriptpath%Player LTV - SWEEPS.twb" -o --db-password !dbpass! -save-db-password  --parent-project-path "New Structure/Loyal Royal/Internal/Players" -r "Valuation" --tabbed
::tabcmd publish "%scriptpath%VIP Users - SWEEPS.twb" -o --db-password !dbpass! -save-db-password  --parent-project-path "New Structure/Loyal Royal/Internal/Players" -r "Valuation" --tabbed
tabcmd publish "%scriptpath%Users to Target for Points - SWEEPS.twb" -o --db-password !dbpass! -save-db-password  --parent-project-path "New Structure/Loyal Royal/Internal/Players" -r "Valuation" --tabbed
::tabcmd publish "%scriptpath%Outstanding Credit Bets - SWEEPS.twb" -o --db-password !dbpass! -save-db-password  --parent-project-path "New Structure/Loyal Royal/Internal/Players" -r "Valuation" --tabbed

::Internal Financials Area
::KPIs
tabcmd publish "%scriptpath%Internal KPI  - SWEEPS.twb" -o --db-password !dbpass! -save-db-password  --parent-project-path "New Structure/Loyal Royal/Internal/Financials" -r "KPIs" --tabbed
tabcmd publish "%scriptpath%Additional KPI Metrics - SWEEPS.twb" -o --db-password !dbpass! -save-db-password --parent-project-path "New Structure/Loyal Royal/Internal/Financials" -r "KPIs" --tabbed

::Publish To Internal Games 
tabcmd publish "%scriptpath%Game Analysis - SWEEPS.twb" -o  --db-password !dbpass! -save-db-password  --parent-project-path "New Structure/Loyal Royal/Internal" -r "Games" --tabbed
tabcmd publish "%scriptpath%Search for Specific Results - SWEEPS.twb" -o --db-password !dbpass! -save-db-password --parent-project-path "New Structure/Loyal Royal/Internal" -r "Games" --tabbed
tabcmd publish "%scriptpath%User Balance Over Time - SWEEPS.twb" -o --db-password !dbpass! -save-db-password  --parent-project-path "New Structure/Loyal Royal/Internal" -r "Games" --tabbed
tabcmd publish "%scriptpath%Daily Stats - SWEEPS.twb" -o --db-password !dbpass! -save-db-password  --parent-project-path "New Structure/Loyal Royal/Internal" -r "Games" --tabbed






::Publish reports to the CLIENT FINANCIALS Folder

::Reconcilliation
tabcmd publish "%scriptpath%Billing Report - SWEEPS.twb" -o --db-password !dbpass! -save-db-password  --parent-project-path "New Structure/Loyal Royal/Client/Financials" -r "Reconcilliation" --tabbed
tabcmd publish "%scriptpath%Transactions Report - SWEEPS.twb" -o --db-password !dbpass! -save-db-password  --parent-project-path "New Structure/Loyal Royal/Client/Financials" -r "Reconcilliation" --tabbed
tabcmd publish "%scriptpath%Daily Redemptions - SWEEPS.twb" -o --db-password !dbpass! -save-db-password  --parent-project-path "New Structure/Loyal Royal/Client/Financials" -r "Reconcilliation" --tabbed
tabcmd publish "%scriptpath%Daily Donations By Player - SWEEPS.twb" -o --db-password !dbpass! -save-db-password  --parent-project-path "New Structure/Loyal Royal/Client/Financials" -r "Reconcilliation" --tabbed

::KPI
tabcmd publish "%scriptpath%Client KPI  - SWEEPS.twb" -o --db-password !dbpass! -save-db-password  --parent-project-path "New Structure/Loyal Royal/Client/Financials" -r "KPIs" --tabbed




::The Rest for Now 
::tabcmd publish "%scriptpath%Jackpots - SWEEPS.twb" -o --db-password !dbpass! -save-db-password  --parent-project-path "New Structure/Loyal Royal/Working" -r "Live" --tabbed


::The comp calculations doesnt work because it uses a local excel file as well.
::tabcmd publish "%scriptpath%Comp Calculations - SWEEPS.twb" -o --db-password !dbpass! -save-db-password  --parent-project-path "New Structure/Loyal Royal/Working" -r " - Working - Live" --tabbed





pause

