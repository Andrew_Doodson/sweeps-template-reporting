# -*- coding: utf-8 -*-
"""
Created on Fri Feb 25 10:38:46 2022

@author: Ruby

Investingating the use of TSC to publish Sweeps reports
"""


import tableauserverclient as TSC
from tableauserverclient import ConnectionCredentials, ConnectionItem
import os



#Establish the filepath and the file that we will be using - this is the directory that the script is in
rootdir =  os.path.dirname(__file__)

#Code token and site credentials here
#When the token changes we only change here intsead of throughout the code
#token_name = 'TSC'
token_name = 'TSC 2'
#token_value = '8KObEq/ASCa3eLmCWWzG1A==:LtHZaw8gjMd8ETMUrGRtqcEq8MJH16zL'
token_value = '9t29e+qHTYq3JgT2XGAZ9g==:Rtq3j5U9a7tWf0AhZSMbHxuWCKgTrLV1'
site_name = 'cashed'


#Have the user input the name of the project that we're publishing to 
publishing_project_name = input("What folder do you want to publish to: ")

#Have the user input the DB password so that it isn't hard coded
db_pass = input("Enter DB Password: ")

#This is our way of signining in using PAT's 
tableau_auth = TSC.PersonalAccessTokenAuth(token_name, token_value, site_name )
server = TSC.Server('https://10ay.online.tableau.com/')



with server.auth.sign_in(tableau_auth):
    #Verify that you've logged in successfully
    print("Successfully logged into server")

    #Step 1: Create a connection
    connection1 = ConnectionItem()    
    connection1.server_address = "loyalroyal-replica.czgungcmn9b3.us-east-2.rds.amazonaws.com"
    connection1.server_port = "5432"
    connection1.connection_credentials = ConnectionCredentials("rubyplay", db_pass, embed = True)
    print('added connection credentials')
    
    all_connections = list()
    all_connections.append(connection1)
    print("Added connection 1 to all connection list")
    
    # Step 2: Get all the projects on server, then look for the specified one.
    all_projects, pagination_item = server.projects.get()
    pub_project = next((project for project in all_projects if project.name == publishing_project_name), None)
    
    #Step 3: Create and publish workbook item(s)
    #Allow publishing to overwrite if there is already a workbook present
    overwrite_true = TSC.Server.PublishMode.Overwrite
    #If we found the  project, start publishing 
    if pub_project is not None:
        print("Found publishing project")
        #Loop through all files in current directory 
        for subdir, dirs, files in os.walk(rootdir):
            for file in files:
                filepath = subdir + os.sep + file            
                #If the filepath ends in twb its a tableau workbook and we want to publish it 
                if filepath.endswith(".twb"):
                    new_workbook = TSC.WorkbookItem(pub_project.id)
                    print(filepath)
                    server.workbooks.publish(new_workbook, filepath, overwrite_true,
                                                   connections = all_connections, skip_connection_check= True) 

                    print('Workbook successfully published')
                    
    else:
        print("Project Not Found")

    
    
    #After we have published every workbook we need to go through and make the views on them tabbed
#    wbs = server.workbooks.get()
#    for wb in wbs:
#        wb.show_tabs = True
#        server.update(wb)

    
    
#Sign out of the server
server.auth.sign_out()
print ("Successfully logged out of server")




