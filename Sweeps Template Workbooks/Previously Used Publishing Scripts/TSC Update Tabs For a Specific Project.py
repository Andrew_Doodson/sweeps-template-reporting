# -*- coding: utf-8 -*-
"""
Created on Fri Mar 11 14:45:00 2022

@author: Ruby
This workbook is to tab all of the workbooks in a specific project
"""
import tableauserverclient as TSC


#When the token changes we only change here intsead of throughout the code
token_name = 'TSC'
token_value = '5cY0iNZJTcSedD585CjOsA==:JveYUypi9jupNsfHW3bcEF2H8T5xygFn'
site_name = 'cashed'

#This is our way of signining in using PAT's 
tableau_auth = TSC.PersonalAccessTokenAuth(token_name, token_value, site_name )
server = TSC.Server('https://10ay.online.tableau.com/')

update_project_name = input("What folder do you want to update: ")


#After we have published every workbook we need to go through and make the views on them tabbed
with server.auth.sign_in(tableau_auth):
    #Get all of the projects on the site 
    all_projects = TSC.Pager(server.projects)
    #Get all of the workbooks on the site 
    all_wbs = TSC.Pager(server.workbooks)
    
    #Filter down to our desired project
    working_project= next((project for project in all_projects if project.name == update_project_name), None)
    
    #If the project the user inputted exists:
    if working_project is not None:
        print('Found project {0}:'.format(working_project.name))
        print()
        print('Project ID is: {0}'.format(working_project.id))
        
        print("Updating project workbooks now...")
        #Filter the wbs list into only the workbooks we want to update
        #If the workbook project id = the working project id we want to update it
        workbooks_to_update = [wb for wb in all_wbs if wb.project_id == working_project.id]
        for wb in workbooks_to_update:
            wb.show_tabs = True
            server.workbooks.update(wb)
            print('Workbook {0} updated'.format(wb.name))
           
        
        #Sign out of the server
        server.auth.sign_out()
        print ("Successfully logged out of server")

    #If the project the user inputted does not exist 
    else:
        print("Project not found")
    