# -*- coding: utf-8 -*-
"""
Created on Wed Mar  9 14:23:03 2022

@author: Ruby
Publish command for sweeps reports

"""

import tableauserverclient as TSC
from tableauserverclient import ConnectionCredentials, ConnectionItem
import os



#Establish the filepath and the file that we will be using - this is the directory that the script is in
rootdir =  os.path.dirname(__file__)

#Code token and site credentials here
#When the token changes we only change here intsead of throughout the code
token_name = 'TSC'
token_value = '5cY0iNZJTcSedD585CjOsA==:JveYUypi9jupNsfHW3bcEF2H8T5xygFn'
site_name = 'cashed'


#Have the user input the name of the project that we're publishing to 
publishing_project_name = input("What folder do you want to publish to: ")

#Have the user input the DB details so that this can be generic
db_username = input("Enter the username for the DB that the workbooks are connected to: ")
server_url = input("Enter server URL that workbooks are currently connected to: ")
db_pass = input("Enter DB Password for this server: ")

#This is our way of signining in using PAT's 
tableau_auth = TSC.PersonalAccessTokenAuth(token_name, token_value, site_name )
server = TSC.Server('https://10ay.online.tableau.com/')



with server.auth.sign_in(tableau_auth):
    #Verify that you've logged in successfully
    print("Successfully logged into server")

    #Step 1: Create a connection
    connection1 = ConnectionItem()    
    connection1.server_address = server_url
    connection1.server_port = "5432"
    connection1.connection_credentials = ConnectionCredentials(db_username, db_pass, embed = True)
    print('added connection credentials')
    
    all_connections = list()
    all_connections.append(connection1)
    print("Added connection 1 to all connection list")
    print("")
    print("")
    # Step 2: Get all the projects on server, then look for the specified one.
    for pj in TSC.Pager(server.projects):
        if pj.name == publishing_project_name:
            
            #Step 3: Create and publish workbook item(s)
            #Allow publishing to overwrite if there is already a workbook present
            overwrite_true = TSC.Server.PublishMode.Overwrite
            #If we found the  project, start publishing 
            
            print("Found publishing project")
            #Loop through all files in current directory 
            for subdir, dirs, files in os.walk(rootdir):
                for file in files:
                    filepath = subdir + os.sep + file            
                    #If the filepath ends in twb its a tableau workbook and we want to publish it 
                    if filepath.endswith(('.twb', '.twbx')):
                        #create a workbook item on the server that we can publish to
                        new_workbook = TSC.WorkbookItem(pj.id)
                        print(filepath)
                        server.workbooks.publish(new_workbook, filepath, overwrite_true,
                                                   connections = all_connections, skip_connection_check= True) 

                        print('Workbook successfully published')
                    
    
#Sign out of the server
server.auth.sign_out()
print ("Successfully logged out of server")




