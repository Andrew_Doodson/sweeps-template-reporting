# -*- coding: utf-8 -*-
"""
Created on Mon Mar 14 15:03:55 2022

@author: Ruby
This script will move reports to other areas on the tableau online server
"""

import tableauserverclient as TSC


#When the token changes we only change here intsead of throughout the code
token_name = 'TSC'
token_value = '5cY0iNZJTcSedD585CjOsA==:JveYUypi9jupNsfHW3bcEF2H8T5xygFn'
site_name = 'cashed'

#Get the folder that the workbooks are currently in 
initial_project_name = input("What folder are the workbooks that need to be moved currently located in: ")

#Get the abbreviation of the project that we want to move the workbooks into
#e.g. Strawberry or Loyal Royal
final_project_name = input("Enter the abreviation project name that you wish to move files to: ")

#This is our way of signining in using PAT's 
tableau_auth = TSC.PersonalAccessTokenAuth(token_name, token_value, site_name )
server = TSC.Server('https://10ay.online.tableau.com/')

with server.auth.sign_in(tableau_auth):
    #Get all of the projects on the site 
    all_projects = TSC.Pager(server.projects)
    #Get all of the workbooks on the site 
    all_wbs = TSC.Pager(server.workbooks)
    
    working_project = next((project for project in all_projects if project.name == initial_project_name), None)
  
    #For some reason we have to get all the projects again to make the second project be found 
    all_projects = TSC.Pager(server.projects)

    final_project = next((project for project in all_projects if project.name == final_project_name), None)

    
    #If the project the user inputted exists:
    if working_project is not None:
        #if the desired project to move to exists
        print('Found project {0}:'.format(working_project.name))
        print()
        print('Project ID is: {0}'.format(working_project.id))
                
        print("Updating project workbooks now...")
        #Filter the wbs list into only the workbooks we want to update
        #If the workbook project id = the working project id we want to update it
        workbooks_to_update = [wb for wb in all_wbs if wb.project_id == working_project.id]
        for wb in workbooks_to_update:
            print("Workbook: {0}".format(wb.name))
            print("Old project: {}".format(wb.project_name))
            wb.project_id = final_project.id
            target_workbook = server.workbooks.update(wb)
            print("New project: {}".format(target_workbook.project_name))
                   
                
        #Sign out of the server
        server.auth.sign_out()
        print ("Successfully logged out of server")
    
    #If the project the user inputted does not exist 
    else:
        print("Project not found")

