@echo off
SETLOCAL EnableDelayedExpansion
::Get Tableau login information from the user
set /p tablogin="Enter Tableau Online Login Email: " 
set /p tabpass="Enter Tableau Online Login Password: "
set /p dbpass="Enter DB Password: "
set /p sitename="Enter Tableau Online Sweeps Site Name: "
::Set the scriptpath to be equal to the folder that the batch file is running in.
set scriptpath=%~dp0

::Login to the site
tabcmd login -s https://10ay.online.tableau.com/#/site/cashed -u %tablogin% -p %tabpass%

::Publish the !sitename! Marketing reports to the !sitename! Internal !sitename! Marketing area
::!sitename! Wins
tabcmd publish "%scriptpath%Big Wins - SWEEPS.twb" -o --db-password !dbpass! -save-db-password --parent-project-path "New Structure/!sitename!/!sitename! Internal/!sitename! Marketing" -r "!sitename! Wins" --tabbed

::Events
::Below doesnt exist for sweeps so it is commented out
::tabcmd publish "%scriptpath%Credit Amount Leaderboard - SWEEPS.twb" -o --db-password !dbpass! -save-db-password  --parent-project-path "New Structure/!sitename!/!sitename! Internal/!sitename! Marketing" -r "Events" --tabbed

::!sitename! Promotions
tabcmd publish "%scriptpath%Player Nicknames - SWEEPS.twb" -o --db-password !dbpass! -save-db-password  --parent-project-path "New Structure/!sitename!/!sitename! Internal/!sitename! Marketing" -r "!sitename! Promotions" --tabbed
tabcmd publish "%scriptpath%Promo Code Report - SWEEPS.twb" -o --db-password !dbpass! -save-db-password  --parent-project-path "New Structure/!sitename!/!sitename! Internal/!sitename! Marketing" -r "!sitename! Promotions" --tabbed

::Doesn't exist, needs making
::tabcmd publish "%scriptpath%Bonus Points Given Away - SWEEPS.twb" -o --db-password !dbpass! -save-db-password  --parent-project-path "New Structure/!sitename!/!sitename! Internal/!sitename! Marketing" -r "!sitename! Promotions" --tabbed

::Publish the !sitename! Players reports to the !sitename! Internal !sitename! Players  area
::!sitename! Risk
tabcmd publish "%scriptpath%Ekata - Risk Profile - SWEEPS.twb" -o --db-password !dbpass! -save-db-password --parent-project-path "New Structure/!sitename!/!sitename! Internal/!sitename! Players" -r "!sitename! Risk" --tabbed

::!sitename! Funnel
tabcmd publish "%scriptpath%Onboarding - SWEEPS.twb" -o --db-password !dbpass! -save-db-password  --parent-project-path "New Structure/!sitename!/!sitename! Internal/!sitename! Players" -r "!sitename! Funnel" --tabbed

::!sitename! Valuation
tabcmd publish "%scriptpath%Daily Stats On Players - SWEEPS.twb" -o --db-password !dbpass! -save-db-password --parent-project-path "New Structure/!sitename!/!sitename! Internal/!sitename! Players" -r "!sitename! Valuation" --tabbed
tabcmd publish "%scriptpath%Game Performance - SWEEPS.twb" -o --db-password !dbpass! -save-db-password  --parent-project-path "New Structure/!sitename!/!sitename! Internal/!sitename! Players" -r "!sitename! Valuation" --tabbed
tabcmd publish "%scriptpath%Player LTV - SWEEPS.twb" -o --db-password !dbpass! -save-db-password  --parent-project-path "New Structure/!sitename!/!sitename! Internal/!sitename! Players" -r "!sitename! Valuation" --tabbed
::tabcmd publish "%scriptpath%VIP Users - SWEEPS.twb" -o --db-password !dbpass! -save-db-password  --parent-project-path "New Structure/!sitename!/!sitename! Internal/!sitename! Players" -r "!sitename! Valuation" --tabbed
tabcmd publish "%scriptpath%Users to Target for Points - SWEEPS.twb" -o --db-password !dbpass! -save-db-password  --parent-project-path "New Structure/!sitename!/!sitename! Internal/!sitename! Players" -r "!sitename! Valuation" --tabbed
::tabcmd publish "%scriptpath%Outstanding Credit Bets - SWEEPS.twb" -o --db-password !dbpass! -save-db-password  --parent-project-path "New Structure/!sitename!/!sitename! Internal/!sitename! Players" -r "!sitename! Valuation" --tabbed

::!sitename! Internal !sitename! Financials Area
::!sitename! KPI
tabcmd publish "%scriptpath%Internal KPI  - SWEEPS.twb" -o --db-password !dbpass! -save-db-password  --parent-project-path "New Structure/!sitename!/!sitename! Internal/!sitename! Financials" -r "!sitename! KPI" --tabbed
tabcmd publish "%scriptpath%Additional KPI Metrics - SWEEPS.twb" -o --db-password !dbpass! -save-db-password --parent-project-path "New Structure/!sitename!/!sitename! Internal/!sitename! Financials" -r "!sitename! KPI" --tabbed

::Publish To !sitename! Internal Games 
tabcmd publish "%scriptpath%Game Analysis - SWEEPS.twb" -o  --db-password !dbpass! -save-db-password  --parent-project-path "New Structure/!sitename!/!sitename! Internal" -r "!sitename! Games" --tabbed
tabcmd publish "%scriptpath%Search for Specific Results - SWEEPS.twb" -o --db-password !dbpass! -save-db-password --parent-project-path "New Structure/!sitename!/!sitename! Internal" -r "!sitename! Games" --tabbed
tabcmd publish "%scriptpath%User Balance Over Time - SWEEPS.twb" -o --db-password !dbpass! -save-db-password  --parent-project-path "New Structure/!sitename!/!sitename! Internal" -r "!sitename! Games" --tabbed
tabcmd publish "%scriptpath%Daily Stats - SWEEPS.twb" -o --db-password !dbpass! -save-db-password  --parent-project-path "New Structure/!sitename!/!sitename! Internal" -r "!sitename! Games" --tabbed
::tabcmd publish "%scriptpath%Jackpots - SWEEPS.twb" -o --db-password !dbpass! -save-db-password  --parent-project-path "New Structure/!sitename!/!sitename! Internal" -r "!sitename! Games" --tabbed






::Publish reports to the CLIENT !sitename! Financials Folder

::!sitename! Reconciliation
tabcmd publish "%scriptpath%Billing Report - SWEEPS.twb" -o --db-password !dbpass! -save-db-password  --parent-project-path "New Structure/!sitename!/!sitename! Client/!sitename! Financials" -r "!sitename! Reconciliation" --tabbed
tabcmd publish "%scriptpath%Daily Redemptions - SWEEPS.twb" -o --db-password !dbpass! -save-db-password  --parent-project-path "New Structure/!sitename!/!sitename! Client/!sitename! Financials" -r "!sitename! Reconciliation" --tabbed
tabcmd publish "%scriptpath%Daily Donations By Player - SWEEPS.twb" -o --db-password !dbpass! -save-db-password  --parent-project-path "New Structure/!sitename!/!sitename! Client/!sitename! Financials" -r "!sitename! Reconciliation" --tabbed
tabcmd publish "%scriptpath%Transactions Report - SWEEPS.twb" -o --db-password !dbpass! -save-db-password  --parent-project-path "New Structure/!sitename!/!sitename! Client/!sitename! Financials" -r "!sitename! Reconciliation" --tabbed


::Here we publish the individual transaction reports based on payment methods that exist for each platform 
set lr=Loyal Royal
:: Publishing to Loyal Royal
if !sitename! == !lr! tabcmd publish "%scriptpath%PayPal Transactions - SWEEPS.twb" -o --db-password !dbpass! -save-db-password  --parent-project-path "New Structure/!sitename!/!sitename! Client/!sitename! Financials" -r "!sitename! Reconciliation" --tabbed
if !sitename! == !lr! tabcmd publish "%scriptpath%Auth Net Transactions - SWEEPS.twb" -o --db-password !dbpass! -save-db-password  --parent-project-path "New Structure/!sitename!/!sitename! Client/!sitename! Financials" -r "!sitename! Reconciliation" --tabbed
if !sitename! == !lr! tabcmd publish "%scriptpath%Pay Near Me Transactions - SWEEPS.twb" -o --db-password !dbpass! -save-db-password  --parent-project-path "New Structure/!sitename!/!sitename! Client/!sitename! Financials" -r "!sitename! Reconciliation" --tabbed

::Publishing to Strawberry
if !sitename! == Strawberry tabcmd publish "%scriptpath%Triple A Transactions - SWEEPS.twb" -o --db-password !dbpass! -save-db-password  --parent-project-path "New Structure/!sitename!/!sitename! Client/!sitename! Financials" -r "!sitename! Reconciliation" --tabbed




::!sitename! KPI
tabcmd publish "%scriptpath%Client KPI  - SWEEPS.twb" -o --db-password !dbpass! -save-db-password  --parent-project-path "New Structure/!sitename!/!sitename! Client/!sitename! Financials" -r "!sitename! KPI" --tabbed


pause

::The Rest for Now 


::The comp calculations doesnt work because it uses a local excel file as well.
::tabcmd publish "%scriptpath%Comp Calculations - SWEEPS.twb" -o --db-password !dbpass! -save-db-password  --parent-project-path "New Structure/!sitename!/Working" -r " - Working - Live" --tabbed






