@echo off
SETLOCAL EnableDelayedExpansion
::Get Tableau login information from the user
set /p tablogin="Enter Tableau Online Login Email: " 
set /p tabpass="Enter Tableau Online Login Password: "
set /p dbpass="Enter DB Password: "
set /p sitename="Enter Tableau Online Sweeps Site Name: "
::Set the scriptpath to be equal to the folder that the batch file is running in.
set scriptpath=%~dp0


::Login to the site
tabcmd login -s https://10ay.online.tableau.com/#/site/cashed -u %tablogin% -p %tabpass%

::Here we publish the individual transaction reports based on payment methods that exist for each platform 
pause
set lr=Loyal Royal
::Here we publish the individual transaction reports based on payment methods that exist for each platform 
echo %lr%
echo %sitename%
pause
if !sitename! == !lr! tabcmd publish "%scriptpath%PayPal Transactions - SWEEPS.twb" -o --db-password !dbpass! -save-db-password  --parent-project-path "New Structure/!sitename!/!sitename! Client/!sitename! Financials" -r "!sitename! Reconciliation" --tabbed
if !sitename! == !lr! tabcmd publish "%scriptpath%Auth Net Transactions - SWEEPS.twb" -o --db-password !dbpass! -save-db-password  --parent-project-path "New Structure/!sitename!/!sitename! Client/!sitename! Financials" -r "!sitename! Reconciliation" --tabbed
if !sitename! == !lr! tabcmd publish "%scriptpath%Pay Near Me Transactions - SWEEPS.twb" -o --db-password !dbpass! -save-db-password  --parent-project-path "New Structure/!sitename!/!sitename! Client/!sitename! Financials" -r "!sitename! Reconciliation" --tabbed


