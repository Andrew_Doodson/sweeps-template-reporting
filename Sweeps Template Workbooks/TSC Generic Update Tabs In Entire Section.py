# -*- coding: utf-8 -*-
"""
Created on Fri Mar 18 15:19:23 2022

@author: Ruby
"""

import tableauserverclient as TSC


#When the token changes we only change here intsead of throughout the code
token_name = 'TSC'
token_value = 'fgNgXYYxT+eIVCOBu8qZYA==:7tG9tAAeTagsM8YHBt8LPThx36z0epYJ'
site_name = 'cashed'

#This is our way of signining in using PAT's 
tableau_auth = TSC.PersonalAccessTokenAuth(token_name, token_value, site_name )
server = TSC.Server('https://10ay.online.tableau.com/')



sections_list = ["Internal KPI" ,"Games", 
                 "Valuation", "Client KPI", 
                 "Promotions", 'Reconciliation',
                 'Risk', 'Funnel', 'Events', 'Wins']


#Get the abbreviation of the publishing project
publishing_project_abbrev = input("Enter the start of the project name that you want to publish to e.g. Strawberry, Loyal Royal: ")


#After we have published every workbook we need to go through and make the views on them tabbed
with server.auth.sign_in(tableau_auth):
    #Loop through each section
    for section in sections_list:
        print(section)
        #Step 2: Get all the projects on server, then look for the specified one.
        for pj in TSC.Pager(server.projects):
            if pj.name == publishing_project_abbrev + " " + section: 
                
                #Get all of the workbooks on the server
                all_wbs = TSC.Pager(server.workbooks)

                workbooks_to_update = [wb for wb in all_wbs if wb.project_id == pj.id]
                for wb in workbooks_to_update:
                    wb.show_tabs = True
                    server.workbooks.update(wb)
                    print('Workbook {0} updated'.format(wb.name))
                
