# -*- coding: utf-8 -*-
"""
Created on Wed Mar 16 15:54:01 2022

@author: Ruby
"""


# -*- coding: utf-8 -*-
"""
Created on Wed Mar  9 14:23:03 2022

@author: Ruby
Publish command for sweeps reports

"""
#Imports required. Install packages using pip install if this fails.
import tableauserverclient as TSC
from tableauserverclient import ConnectionCredentials, ConnectionItem
import os


########################### HARD CODED SECTION ###############################
#Here we have the portions of code that are hard coded once at the begining 
#That way we can update once here if anything does need updating.

#Establish the filepath and the file that we will be using
#This is the directory that the script is in. Shouldn't need updating.
rootdir =  os.path.dirname(__file__)

#PAT credentials coded here. UPDATE if PAT expires.
token_name = 'TSC'
token_value = 'fgNgXYYxT+eIVCOBu8qZYA==:7tG9tAAeTagsM8YHBt8LPThx36z0epYJ'
site_name = 'cashed'


#Lists of each workbook and which project they belong to.
#If new workbooks are made, update this list only.  

#CLIENT FOLDER

#Client -> Financials 
#Client -> Financials -> Reconciliation
reconciliation = ['Auth Net Transactions - SWEEPS.twb', 'Billing Report - SWEEPS.twb',
                  'Daily Donations By Player - SWEEPS.twb', 'Daily Redemptions - SWEEPS.twb', 
                  'PayPal Transactions - SWEEPS.twb', 'Transactions Report - SWEEPS.twb',
                  'PNM Processing Fees - SWEEPS.twb'] 

##Client -> Financials -> Client KPI  
#(Folder needs to be renamed ain online from KPI to Client KPI)
client_kpi = ['Client KPI  - SWEEPS.twb']

#INTERNAL FOLDER

#Internal Financials
#Internal Financials -> Internal KPI
#This needs to be renamed in the online structure.)
internal_kpi = ["Internal KPI  - SWEEPS.twb", "Additional KPI Metrics - SWEEPS.twb",
                "Product Owner meeting Template - Sweeps.twb", "Funnels - Sweeps.twb"]

#Internal Games
internal_games = ["Game Analysis - SWEEPS.twb", "Search for Specific Results - SWEEPS.twb",
                  "User Balance Over Time - SWEEPS.twb", "Daily Stats - SWEEPS.twb"]

#Internal Players 
#Internal Players -> Valuation
valuation = ["Daily Stats On Players - SWEEPS.twb", "Game Performance - SWEEPS.twb", "Player LTV - SWEEPS.twb",
             "VIP Users - SWEEPS.twb", "Users to Target for Points - SWEEPS.twb", "Outstanding Credit Bets - SWEEPS.twb"]

#Internal Players -> Risk 
risk = ['Ekata - Risk Profile - SWEEPS.twb', 'Smart Bonus Hunters - SWEEPS.twb']

#Internal -> Players -> Funnel
funnel = ['Onboarding - SWEEPS.twb', 'User Attribution Viz - Sweeps.twb','Funnels - Sweeps.twb']


#Internal -> Marketing
#Internal -> Marketing -> Promotions
promotions = ["Player Nicknames - SWEEPS.twb", "Promo Code Report - SWEEPS.twb" ,
              "Cashback Report - SWEEPS.twb"]

#Internal -> Marketing -> Events
events = []

#Internal -> Marketing -> Wins
wins = ['Big Wins - SWEEPS.twb']



#Dictionary of all of these sections 
#Means that we can have the name of the section to append to the abbreviation that the user inputs
#and the list of workbooks that belong in each section 
sections_dict = {"Internal KPI": internal_kpi,"Games": internal_games, 
                 "Valuation": valuation, "Client KPI": client_kpi, 
                 "Promotions": promotions, 'Reconciliation': reconciliation,
                 'Risk': risk, 'Funnel': funnel, 'Events': events, 'Wins': wins}

##############################################################################

#Have the user input the DB details for the workbook datasource(s)

server_url = input("Enter database server URL that workbooks are currently connected to: ")
db_username = input("Enter the username for the DB that the workbooks are connected to: ")
db_pass = input("Enter DB Password for this server: ")


#Get the abbreviation of the project that we want to move the workbooks into
#e.g. Strawberry or Loyal Royal 
publishing_project_abbrev = input("Enter the start of the project name that you want to publish to e.g. Strawberry, Loyal Royal: ")


#Sign in to server using PAT 
tableau_auth = TSC.PersonalAccessTokenAuth(token_name, token_value, site_name )
server = TSC.Server('https://10ay.online.tableau.com/')

with server.auth.sign_in(tableau_auth):
    #Verify that you've logged in successfully
    print("Successfully logged into server")

    #Step 1: Create a connection item for the workbooks
    connection1 = ConnectionItem()    
    connection1.server_address = server_url
    connection1.server_port = "5432"
    connection1.connection_credentials = ConnectionCredentials(db_username, db_pass, embed = True)
    print('added connection credentials')
    
    all_connections = list()
    all_connections.append(connection1)
    print("Added connection 1 to all connection list")
    print("")
    print("")
    
    #Step 2: Loop through each group of workbooks for each section
    for name, sublist in sections_dict.items():
        #Step 2: Get all the projects on server, then look for the specified one.
        for pj in TSC.Pager(server.projects):
            if pj.name == publishing_project_abbrev + " " + name:                
                #Step 3: Create and publish workbook item(s)
                #Allow publishing to overwrite if there is already a workbook present
                overwrite_true = TSC.Server.PublishMode.Overwrite
                #If we found the  project, start publishing 
                
                print("Found publishing project")
                print(pj.name)
                print()
                print()
                #Loop through all files in current directory 
                for subdir, dirs, files in os.walk(rootdir):
                    for file in files:
                        filepath = subdir + os.sep + file            
                        #If the filepath ends in twb its a tableau workbook and we want to publish it 

                        if filepath.endswith(('.twb', '.twbx')) and file in sublist:
                            #create a workbook item on the server that we can publish to
                            new_workbook = TSC.WorkbookItem(pj.id)
                            print("Publishing workbook: " + file)
                            server.workbooks.publish(new_workbook, filepath, overwrite_true,
                                                       connections = all_connections, skip_connection_check= True) 
    
                            print('Workbook successfully published')
                            print()
                        
    
#Sign out of the server
server.auth.sign_out()
print ("Successfully logged out of server")


