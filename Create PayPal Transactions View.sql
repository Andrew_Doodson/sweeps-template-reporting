-- View: sweeps.paypal_transactions

-- DROP VIEW sweeps.paypal_transactions;

CREATE OR REPLACE VIEW sweeps.paypal_transactions
 AS
 SELECT a.action_id,
    a.paypal_order_id,
    a.paypal_capture_id,
    a.created_at,
    a.last_modified_at,
    b.email,
    d.cash_amount,
    d.payment_provider,
    d.status AS donation_status,
    c.status AS action_status
   FROM sweeps.paypal a,
    sweeps.users b,
    sweeps.user_actions c,
    sweeps.payments d
  WHERE a.action_id = c.action_id AND b.user_id::text = c.user_id::text AND a.action_id = d.action_id
  ORDER BY a.created_at DESC;

ALTER TABLE sweeps.paypal_transactions
    OWNER TO rubyplay;

GRANT ALL ON TABLE sweeps.paypal_transactions TO rubyplay;

