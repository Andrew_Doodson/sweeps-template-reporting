-- View: sweeps.Cashouts and  Payment Union View 

-- DROP VIEW sweeps."Cashouts and  Payment Union View ";

CREATE OR REPLACE VIEW sweeps."Cashouts and  Payment Union View "
 AS
 SELECT a.user_id,
    b.email,
    a.created_at,
    a.last_modified_at,
    a.sweep_tickets_amount,
    a.cash_amount,
    a.status,
    c.action_type,
    a.payment_provider,
    a.promo_id
   FROM sweeps.payments a,
    sweeps.users b,
    sweeps.user_actions c
  WHERE a.user_id::text = b.user_id::text AND a.action_id = c.action_id
UNION
 SELECT a.user_id,
    b.email,
    a.created_at,
    a.last_modified_at,
    a.sweep_tickets_amount,
    a.sweep_tickets_amount / 100 AS cash_amount,
    a.status,
    c.action_type,
    NULL::character varying AS payment_provider,
    NULL::uuid AS promo_id
   FROM sweeps.cash_outs a,
    sweeps.users b,
    sweeps.user_actions c
  WHERE a.user_id::text = b.user_id::text AND a.action_id = c.action_id;

ALTER TABLE sweeps."Cashouts and  Payment Union View "
    OWNER TO rubyplay;
COMMENT ON VIEW sweeps."Cashouts and  Payment Union View "
    IS 'This one is the new and updated one ';

GRANT SELECT ON TABLE sweeps."Cashouts and  Payment Union View " TO rubyplay;
GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE sweeps."Cashouts and  Payment Union View " TO rubyplay;
GRANT ALL ON TABLE sweeps."Cashouts and  Payment Union View " TO rubyplay;

