-- View: sweeps.cashout_donation_union_view

-- DROP VIEW sweeps.cashout_donation_union_view;

CREATE OR REPLACE VIEW sweeps.cashout_donation_union_view
 AS
 SELECT "cashout_actionID_view".user_id,
    "cashout_actionID_view".action_id,
    "cashout_actionID_view".action_type,
    "cashout_actionID_view".parent_action_id,
    "cashout_actionID_view".created_at,
    "cashout_actionID_view".last_modified_at,
    "cashout_actionID_view".email,
    "cashout_actionID_view".cashout_status AS status,
    "cashout_actionID_view".sweep_tickets_amount AS amount,
    "cashout_actionID_view".cashout_last_modified_at AS cashout_or_donation_last_modified
   FROM sweeps."cashout_actionID_view"
UNION
 SELECT "donation_actionID_view".user_id,
    "donation_actionID_view".action_id,
    "donation_actionID_view".action_type,
    "donation_actionID_view".parent_action_id,
    "donation_actionID_view".created_at,
    "donation_actionID_view".last_modified_at,
    "donation_actionID_view".email,
    "donation_actionID_view".donation_status AS status,
    "donation_actionID_view".donation_cash_amount AS amount,
    "donation_actionID_view".donation_last_modified_at AS cashout_or_donation_last_modified
   FROM sweeps."donation_actionID_view"
  ORDER BY 3;

ALTER TABLE sweeps.cashout_donation_union_view
    OWNER TO rubyplay;

GRANT SELECT ON TABLE sweeps.cashout_donation_union_view TO rubyplay;
GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE sweeps.cashout_donation_union_view TO rubyplay;
GRANT ALL ON TABLE sweeps.cashout_donation_union_view TO rubyplay;

