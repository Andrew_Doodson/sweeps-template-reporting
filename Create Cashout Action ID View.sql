-- View: sweeps.cashout_actionID_view

-- DROP VIEW sweeps."cashout_actionID_view";

CREATE OR REPLACE VIEW sweeps."cashout_actionID_view"
 AS
 SELECT a.user_id,
    a.action_id,
    a.action_type,
    a.status,
    a.parent_action_id,
    a.created_at,
    a.last_modified_at,
    b.email,
    c.last_modified_at AS cashout_last_modified_at,
    c.sweep_tickets_amount,
    c.status AS cashout_status
   FROM sweeps.user_actions a,
    sweeps.users b,
    sweeps.cash_outs c
  WHERE a.user_id::text = b.user_id::text AND a.action_id = c.action_id
  ORDER BY a.created_at;

ALTER TABLE sweeps."cashout_actionID_view"
    OWNER TO rubyplay;

GRANT SELECT ON TABLE sweeps."cashout_actionID_view" TO rubyplay;
GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE sweeps."cashout_actionID_view" TO rubyplay;
GRANT ALL ON TABLE sweeps."cashout_actionID_view" TO rubyplay;

