-- View: sweeps.bet_transactions_view

-- DROP VIEW sweeps.bet_transactions_view;

CREATE OR REPLACE VIEW sweeps.bet_transactions_view
 AS
 SELECT a.action_id,
    a.bet_amount,
    a.bet_id,
    a.currency,
    a.game_name,
    a.paytable_used,
    a.status,
    a.win_amount,
    a.win_type,
    a.created_at,
    a.last_modified_at,
    a.user_id,
    a.sweepstake_start_date,
    a.sweepstake_end_date,
    a.sweepstake_current_position,
    b.email
   FROM sweeps.bets a,
    sweeps.users b
  WHERE a.user_id::text = b.user_id::text
  ORDER BY a.created_at DESC;

ALTER TABLE sweeps.bet_transactions_view
    OWNER TO rubyplay;

GRANT ALL ON TABLE sweeps.bet_transactions_view TO rubyplay;

