-- View sweeps.user_verification_status_view

-- DROP VIEW sweeps.user_verification_status_view;

CREATE OR REPLACE VIEW sweeps.user_verification_status_view
 AS
 SELECT user_phone_verifications.verification_id,
    user_phone_verifications.user_id,
    user_phone_verifications.created_at,
    user_phone_verifications.email,
    user_phone_verifications.phone_number,
    user_phone_verifications.status,
    user_phone_verifications.is_match,
    user_phone_verifications.action_id,
    user_phone_verifications.raw_responsejson - 'phone.line_type'text AS phone_line_type,
    (user_phone_verifications.raw_response - 'email.valid'text)boolean AS email_valid,
    user_phone_verifications.raw_responsejson - 'email.to_name'text AS email_to_name,
    (user_phone_verifications.raw_response - 'email.first_seen_days'text)integer AS email_first_seen_days,
    (user_phone_verifications.raw_response - 'phone.valid'text)boolean AS phone_valid,
    user_phone_verifications.raw_responsejson - 'phone.carrier'text AS phone_carrier,
    user_phone_verifications.raw_responsejson - 'phone.country_code'text AS phone_country_code,
    (user_phone_verifications.raw_response - 'phone.last_seen_days'text)integer AS phone_last_seen_days,
    (user_phone_verifications.raw_response - 'phone.email.first_seen_days'text)integer AS phone_email_first_seen_days,
    user_phone_verifications.raw_responsejson - 'phone.to_name'text AS phone_to_name,
    (user_phone_verifications.raw_response - 'identity_network_score'text)real AS identity_network_score,
    (user_phone_verifications.raw_response - 'identity_risk_score'text)integer AS identity_risk_score
   FROM sweeps.user_phone_verifications;

ALTER TABLE sweeps.user_verification_status_view
    OWNER TO rubyplay;

GRANT SELECT ON TABLE sweeps.user_verification_status_view TO rubyplay;
GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE sweeps.user_verification_status_view TO rubyplay;
GRANT ALL ON TABLE sweeps.user_verification_status_view TO rubyplay;

