-- View: sweeps.auth_net_transactions

-- DROP VIEW sweeps.auth_net_transactions;

CREATE OR REPLACE VIEW sweeps.auth_net_transactions
 AS
 SELECT a.action_id,
    a.created_at,
    a.last_modified_at,
    a.invoice_number,
    a.transaction_id,
    b.email,
    d.cash_amount,
    d.payment_provider,
    d.status AS donation_status,
    c.status AS action_status
   FROM sweeps.authorize_net a,
    sweeps.users b,
    sweeps.user_actions c,
    sweeps.payments d
  WHERE a.action_id = c.action_id AND b.user_id::text = c.user_id::text AND a.action_id = d.action_id
  ORDER BY a.created_at DESC;

ALTER TABLE sweeps.auth_net_transactions
    OWNER TO rubyplay;

GRANT ALL ON TABLE sweeps.auth_net_transactions TO rubyplay;

