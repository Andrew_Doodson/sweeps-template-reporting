-- View: sweeps.Bonuses View

-- DROP VIEW sweeps."Bonuses View";

CREATE OR REPLACE VIEW sweeps."Bonuses View"
 AS
 SELECT a.action_id,
    a.additional_notes,
    a.accounts_adjustment_type,
    a.transaction_id,
    a.created_at,
    a.last_modified_at,
    b.amount AS points_amount,
    c.email,
    'dashboard_bonus'::character varying AS promo_code,
    'dashboard_bonus'::character varying AS voucherify_type
   FROM sweeps.accounts_adjustments a,
    sweeps.sweep_tickets_transactions b,
    sweeps.users c
  WHERE a.action_id = b.action_id AND b.user_id::text = c.user_id::text AND a.accounts_adjustment_type::text <> 'Null'::text
UNION ALL
 SELECT a.action_id,
    NULL::character varying AS additional_notes,
    NULL::character varying AS accounts_adjustment_type,
    NULL::uuid AS transaction_id,
    a.created_at,
    a.last_modified_at,
    a.points_amount,
    b.email,
    a.promo_code,
    a.voucherify_type
   FROM sweeps.promos a,
    sweeps.users b,
    sweeps.user_actions c
  WHERE a.action_id = c.action_id AND b.user_id::text = c.user_id::text AND a.promo_code::text <> 'DEFAULT'::text;

ALTER TABLE sweeps."Bonuses View"
    OWNER TO rubyplay;

GRANT SELECT ON TABLE sweeps."Bonuses View" TO rubyplay;
GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE sweeps."Bonuses View" TO rubyplay;
GRANT ALL ON TABLE sweeps."Bonuses View" TO rubyplay;

